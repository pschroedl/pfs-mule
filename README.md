PFS-Mule

A simple Url Lookup Service implemented in NodeJS/MongoDB/Mongoose that responds to GET requests where the caller passes in a URL and the service responds with **some information about that URL**. The GET
requests are in the format of:
 
        GET /urlinfo/1/{hostname_and_port}/{original_path_and_query_string}


Running node server.js from the PFS-Mule folder will start the running on localhost:8080.  It is currently configured to access a MongoDB server running at localhost:27017, with a database named muledb.

Additional Considerations/TODO:
 
  Implement sharding utilizing MongoDB's built in support, which would allow for horizontal scaling to accomidate for any size database.

  To scale for large #s of requests, either utilize a service for load balancing  ( such as in AWS Elastic Beanstalk ), or create a load balancing app in NodeJS using seaport to delegate requests to separate server instances running a modified version of PFS-Mule that register automatically with the load balancing app as a helper.
