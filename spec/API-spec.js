// SERVER SIDE TEST SUITE
var request = require('superagent'),
var expect  = require('../expect.js'),
var app     = require('../server.js');

// BEFORE HOOK
before(function(){
  this.server = http.createServer(app.API.handleRequest).listen(3000);
});

// AFTER HOOK
after(function(done){
  this.server.close(done);
});

describe('API: GET /urlinfo/1/', function() {
  it('Should Return 200', function(done) {
    request.get('localhost:3000/urlinfo/1/').end(function(res) {
      expect(res).to.exist;
      expect(res.status).to.equal(200);
      done();
    });
  });
});

describe('API: GET /urlinfo/1/www.netflix.com', function() {
  it('Should Return 200', function(done) {
    request.get('localhost:3000/urlinfo/1/www.netflix.com').end(function(res) {
      expect(res).to.exist;
      expect(res.status).to.equal(200);
      done();
    });
  });
});

describe('API: GET /invalidendpoint', function() {
  it('Should Return 400', function(done) {
    request.get('localhost:3000/invalidendpoint').end(function(res) {
      expect(res).to.exist;
      expect(res.status).to.equal(400);
      done();
    });
  });
});
