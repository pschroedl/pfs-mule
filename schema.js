var mongoose =     require('mongoose');
var Schema =       mongoose.Schema;

var URLSchema = new Schema({
  blockedURL : String,
  hostname : String,
  port : Number,
  path : String,
  queryString : String
});

var URLModel = mongoose.model('URL', URLSchema);
module.exports.URLModel = URLModel;