var config = require("./config");
var server = require('./server');
var schema = require("./schema");

var handleRequest = function (req, res) {
  if(req.method === "GET"){
    getHandler(req, res);
  } else {
    errHandler(res);
  }
};

var sendResponse = function(response, obj, status, type){
  status = status || 200;
  config.headers['Content-Type'] = type || "text/html";
  response.writeHead(status, config.headers);
  response.end(obj);
};

var getHandler = function(req, res){
  var endPoint = req.url.slice(0,11);
  var lookupURL = req.url.slice(11);

  if (endPoint !== '/urlinfo/1/'){
    errHandler(res);
    return;
  }

  schema.URLModel.findOne({'blockedURL': lookupURL },
    'blockedURL',
    function(err, result){
    if (result === null){
      sendResponse(res,"Non-Blocked Site");
    } else {
      sendResponse(res, result.blockedURL + " is blacklisted");
    }
  });
};

var errHandler = function(res){
  sendResponse(res,"Bad Request", 400);
};

module.exports.handleRequest = handleRequest;