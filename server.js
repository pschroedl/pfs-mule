var http      = require("http");
var mongoose  = require('mongoose');
var API       = require("./API");
var config    = require("./config");

mongoose.connect('mongodb://' 
  + config.database.IP + ':' 
  + config.database.port + '/' 
  + config.database.name);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'DB connection error:'));
db.once('open', function callback(){
  console.log('Connected to ' + config.database.name);
});

var server = http.createServer(API.handleRequest);
server.listen(config.server.port, config.server.IP);
console.log("Listening on http://"
  + config.server.IP + ":" 
  + config.server.port);