exports.server = {
  port : 8080,
  IP : "127.0.0.1"
}

exports.database = {
  name : "muledb",
  port : 27017,
  IP : "localhost"
}

exports.headers = {
  "access-control-allow-origin": "*",
  "access-control-allow-methods": "GET",
  "Access-Control-Allow-Headers" : "X-Requested-With, X-HTTP-Method-Override,  Content-Type, Accept",
  "access-control-max-age": 10, // Seconds.
  'Content-Type' : "text/html"
};
